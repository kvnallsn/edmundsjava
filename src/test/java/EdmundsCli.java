package allison.kevin.cse691.car;

import java.io.IOException;

import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.Retrofit;

import allison.kevin.cse691.car.EdmundsApi;
import allison.kevin.cse691.car.cache.LocalCache;
import allison.kevin.cse691.car.cache.SqliteCache;
import allison.kevin.cse691.car.models.Car;
import allison.kevin.cse691.car.models.Make;
import allison.kevin.cse691.car.models.Model;
import allison.kevin.cse691.car.models.Year;
import allison.kevin.cse691.car.models.Style;
import allison.kevin.cse691.car.models.FullStyle;
import allison.kevin.cse691.car.models.Engine;
import allison.kevin.cse691.car.models.Transmission;
import allison.kevin.cse691.car.utilities.MaintInfo;

public final class EdmundsCli {

  public static int get_choice(String prompt) {
    System.out.print(prompt + ": ");

    Scanner reader = new Scanner(System.in);
    int choice = reader.nextInt();

    System.out.println();

    return choice;
  }

  public static int print_menu(List<?> list) {
    int listPos;
    for (listPos = 1; listPos <= list.size(); listPos++) {
      Object obj = list.get(listPos - 1);
      String formattedName = String.format("%2d) %-25s", listPos - 1,  obj);
      System.out.print(formattedName);
      if (listPos % 4 == 0) {
        System.out.println();
      }
    }

    System.out.println();

    return get_choice("Choice");
  }

  public static <T> int print_menu_one_per_line(List<T> list) {
    int listPos;
    for (listPos = 0; listPos < list.size(); listPos++) {
      T obj = list.get(listPos);
      String formattedName = String.format("%2d) %-25s", listPos,  obj);
      System.out.println(formattedName);
    }

    System.out.println();

    return get_choice("Choice");
  }

  public static <T> void print(String header, List<T> list) {
    System.out.println();
    System.out.println(header);
    System.out.println("-------------------------------");
    for (T obj : list) {
      System.out.println(obj);
    }
  }

  public static void main(String... args) throws IOException {
    // Create a simple REST adapter to point to API
    Retrofit retrofit = new Retrofit.Builder()
        .baseUrl(EdmundsApi.API_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build();

    // Create instance of the API interface
    Edmunds edmunds = retrofit.create(Edmunds.class);

    LocalCache cache = new SqliteCache();
    EdmundsApi.setCache(cache);


    while (true) {
      System.out.println("My Garage");
      System.out.println("--------------");
      List<Car> garage = cache.getMyGarage();
      int pos = 0;
      for (Car car : garage) {
        String formatted = String.format("%2d) %-25s", pos++, car.toString());
        System.out.println(formatted);
      }

      System.out.println("");
      System.out.println("----");
      System.out.println("1) Add Car");
      System.out.println("2) Delete Car");
      System.out.println("3) View Car");
      System.out.println("4) Exit");

      switch (get_choice(">")) {
        case 1:
          addCar(edmunds, cache);
          break;
        case 2: {
          int choice = get_choice(">>");
          if ((choice < 0) || (choice > garage.size())) {
            System.err.println("Invalid Choice");
            break;
          }
          cache.deleteCarFromGarage(garage.get(choice));
          break;
        }
        case 3: {
          int choice = get_choice(">>");
          if ((choice < 0) || (choice > garage.size())) {
            System.err.println("Invalid Choice");
            break;
          }
          // Need to subtract one because SQLite starts at 1, not 0
          viewCar(edmunds, garage.get(choice));
          break;
        }
        case 4:
          System.exit(0);
          break;
        default:
          break;
      }
    }
  }

  private static void addCar(Edmunds edmunds, LocalCache cache) throws IOException {

    List<Make> makes = EdmundsApi.getMakes(edmunds);
    int choice = print_menu(makes);

    final Make make = makes.get(choice);
    choice = print_menu(make.models);

    final Model model = make.models.get(choice);
    choice = print_menu(model.years);

    final Year year = EdmundsApi.getModelYearInfo(edmunds, 
        make, model, model.years.get(choice).year);
    choice = print_menu_one_per_line(year.styles);

    final Style style = year.styles.get(choice);
    final FullStyle fullStyle = EdmundsApi.getStyleInfo(edmunds, style.id);
    final Engine engine = fullStyle.engine;
    final Transmission trans = fullStyle.transmission;

    // Add to garage
    cache.addToGarage(new Car(0, make.name, model.name, year.year,
          style.toString(), year.id, style.id, engine.code, trans.transmissionType));
  }

  private static void viewCar(Edmunds edmunds, Car car) throws IOException {
    while (true) {
      String selected = String.format("%s (%d) (%d)", car, car.modelYearId, car.styleId);
      System.out.println("Car: " + selected);

      System.out.println("1) Recalls");
      System.out.println("2) Bullitens");
      System.out.println("3) Notes");
      System.out.println("4) Manufacturers Maintenance Schedule");
      System.out.println("5) Back");

      switch (get_choice(">>>")) {
        case 1:
          print("Recalls", EdmundsApi.getRecalls(edmunds, car.modelYearId));
          break;
        case 2:
          print("Bulletins", EdmundsApi.getBulletins(edmunds, car.modelYearId));
          break;
        case 3:
          print("Maintenance Notes", EdmundsApi.getMaintenanceNotes(edmunds, car.styleId));
          break;
        case 4: {
          //Map<Integer, List<Maintenance>> buckets = EdmundsApi.getMaintenance(
          MaintInfo info = EdmundsApi.getMaintenance(
              edmunds, car.modelYearId, car.engine, car.transmission);

          for (Integer bucket : info.mileage.keySet()) {
            print(String.format("%d Miles", bucket), info.mileage.get(bucket));
            System.out.println();
          }
          System.out.println("--------------------------------------");
          for (Integer bucket : info.months.keySet()) {
            print(String.format("%d Months", bucket), info.months.get(bucket));
            System.out.println();
          }
          break;
        }
        case 5:
          return;
        default:
          break;
      }
      System.out.println();
    }
  }
}
