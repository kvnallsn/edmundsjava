package allison.kevin.cse691.car;

import java.util.List;

import allison.kevin.cse691.car.models.Make;
import allison.kevin.cse691.car.models.Recall;
import allison.kevin.cse691.car.models.ServiceBulletin;
import allison.kevin.cse691.car.models.MaintenanceNote;
import allison.kevin.cse691.car.models.Engine;

class Holder<T> {
  public final List<T> holder;

  public Holder(List<T> holder) {
    this.holder = holder;
  }
}

class Makes {
  public List<Make> makes;

  public Makes(List<Make> makes) {
    this.makes = makes;
  }
}

class RecallHolder {
  public final List<Recall> recallHolder;

  public RecallHolder(List<Recall> recallHolder) {
    this.recallHolder = recallHolder;
  }
}

class ServiceBulletinHolder {
  public final List<ServiceBulletin> serviceBulletinHolder;

  public ServiceBulletinHolder(List<ServiceBulletin> serviceBulletinHolder) {
    this.serviceBulletinHolder = serviceBulletinHolder;
  }
}

class MaintenanceNotesHolder {
  public final List<MaintenanceNote> maintenanceStyleNotesHolder;

  public MaintenanceNotesHolder(List<MaintenanceNote> maintenanceStyleNotesHolder) {
    this.maintenanceStyleNotesHolder = maintenanceStyleNotesHolder;
  }
}

class EngineHolder {
  public final List<Engine> engines;

  public EngineHolder(List<Engine> engines) {
    this.engines = engines;
  }
}
