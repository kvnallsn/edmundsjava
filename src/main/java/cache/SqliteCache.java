package allison.kevin.cse691.car.cache;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import java.util.ArrayList;
import java.util.List;

import allison.kevin.cse691.car.models.Car;
import allison.kevin.cse691.car.models.Maintenance;

public class SqliteCache implements LocalCache {

  private Connection conn;

  public SqliteCache() {
    try {
      Class.forName("org.sqlite.JDBC");
      conn = DriverManager.getConnection("jdbc:sqlite:car.db");
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
    }

    createGarage();
    createMaintenanceCacheTable();
    createMaintenanceTable();
  }

  private void createGarage() {
    Statement stmt;
    try {
      stmt = conn.createStatement();
      String sql = "CREATE TABLE IF NOT EXISTS garage ("
          + "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
          + "make TEXT NOT NULL, " 
          + "model TEXT NOT NULL, " 
          + "year TEXT NOT NULL, "
          + "trim TEXT NOT NULL, "
          + "modelyearid INTEGER NOT NULL, "
          + "styleid INTEGER NOT NULL, "
          + "engine TEXT NOT NULL, "
          + "transmission TEXT NOT NULL)";

      stmt.executeUpdate(sql);
      stmt.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
    }
  }

  private void createMaintenanceCacheTable() {
    Statement stmt;
    try {
      stmt = conn.createStatement();
      String sql = "CREATE TABLE IF NOT EXISTS maint_cache ("
          + "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
          + "modelyearid INTEGER NOT NULL)";

      String msql = "CREATE TABLE IF NOT EXISTS maintenance ( "
          + "id INTEGER PRIMARY KEY NOT NULL, "
          + "modelyearid INTEGER NOT NULL, "
          + "engine TEXT NOT NULL, "
          + "trans TEXT NOT NULL, "
          + "mileage INTEGER NOT NULL, "
          + "month INTEGER NOT NULL, "
          + "freq INTEGER NOT NULL, "
          + "action TEXT NOT NULL, "
          + "item TEXT NOT NULL, "
          + "desc TEXT NOT NULL, "
          + "drive TEXT NOT NULL)";

      stmt.executeUpdate(sql);
      stmt.executeUpdate(msql);

      stmt.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
    } 
  }

  private void createMaintenanceTable() {
    Statement stmt;
    try {
      stmt = conn.createStatement();
      String sql = "CREATE TABLE IF NOT EXISTS my_maintenance ( "
          + "id INTEGER NOT NULL, "
          + "maintenanceId INTEGER NOT NULL, "
          + "mileage INTEGER NOT NULL, "
          + "date TEXT NOT NULL)";

      stmt.executeUpdate(sql);
      stmt.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
    } 
  }

  public List<Car> getMyGarage() {
    List<Car> garage = new ArrayList<Car>();

    Statement stmt;
    try {
      stmt = conn.createStatement();
      String sql = "SELECT * FROM garage";

      ResultSet rs = stmt.executeQuery(sql);

      while (rs.next()) {
        garage.add(new Car(
              rs.getInt("id"),
              rs.getString("make"),
              rs.getString("model"),
              rs.getInt("year"),
              rs.getString("trim"),
              rs.getInt("modelyearid"),
              rs.getInt("styleid"),
              rs.getString("engine"),
              rs.getString("transmission")));
      }

      rs.close();
      stmt.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
    }

    return garage;
  }

  public Car getCarFromGarage(int id) {
    Car car = null;
    PreparedStatement stmt;
    try {
      stmt = conn.prepareStatement("SELECT * FROM garage WHERE id=?");
      stmt.setInt(1, id);

      ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        car = new Car(
            rs.getInt("id"),
            rs.getString("make"),
            rs.getString("model"),
            rs.getInt("year"),
            rs.getString("trim"),
            rs.getInt("modelyearid"),
            rs.getInt("styleid"),
            rs.getString("engine"),
            rs.getString("transmission"));
      }

      rs.close();
      stmt.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
    }

    return car;
  }
  
  public boolean checkMaintenanceCache(int modelYearId) {
    boolean ret = false;
    PreparedStatement stmt;
    try {
      stmt = conn.prepareStatement("SELECT * FROM maint_cache WHERE modelyearid=?");
      stmt.setInt(1, modelYearId);

      ResultSet rs = stmt.executeQuery();

      ret = (rs.next() && (rs.getInt("modelyearid") == modelYearId));
      stmt.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
    }

    System.out.println("Car (" + modelYearId + ") in cache? " + (ret ? "Yes" : "No"));
    return ret;
  }

  public List<Maintenance> getMaintenance(int modelYearId, String[] filterCols, String[] filterArgs) {
    List<Maintenance> lst = new ArrayList<Maintenance>();

    Statement stmt;
    try {
      stmt = conn.createStatement();

      String sql = String.format("SELECT * FROM maintenance WHERE modelyearid=%d", modelYearId);
      ResultSet rs = stmt.executeQuery(sql);

      while (rs.next()) {
        Maintenance maint = new Maintenance(
            rs.getInt("id"),
            rs.getInt("edmundsId"),
            rs.getString("engine"),
            rs.getString("trans"),
            rs.getInt("mileage"),
            rs.getInt("month"),
            rs.getInt("freq"),
            rs.getString("action"),
            rs.getString("item"),
            rs.getString("desc"),
            0.0f, 0.0f,
            rs.getString("drive"),
            "", "");

        lst.add(maint);
      }

      rs.close();
      stmt.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
    }

    return lst;
  }

  public List<Maintenance> getMaintenance(int modelYearId) {
    return getMaintenance(modelYearId, null, null);
  }

  public void addToGarage(Car car) {
  
    try {
      PreparedStatement stmt = conn.prepareStatement("INSERT INTO garage (make, model, year, "
          + "trim, modelyearid, styleid, engine, transmission) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");

      stmt.setString(1, car.make);
      stmt.setString(2, car.model);
      stmt.setInt(3, car.year);
      stmt.setString(4, car.trim);
      stmt.setInt(5, car.modelYearId);
      stmt.setInt(6, car.styleId);
      stmt.setString(7, car.engine);
      stmt.setString(8, car.transmission);

      stmt.executeUpdate();
      stmt.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
    }
  }

  public void deleteCarFromGarage(Car car) {
    try {
      PreparedStatement stmt = conn.prepareStatement("DELETE FROM garage WHERE id=?");
      stmt.setInt(1, car.id);

      stmt.executeUpdate();
      stmt.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
    }
  }

  public void cacheMaintenance(int modelYearId, List<Maintenance> maintList) {

    try {
      conn.setAutoCommit(false);    // Group commits
      for (Maintenance maint : maintList) {
        PreparedStatement stmt = conn.prepareStatement("INSERT INTO maintenance "
            + "(id, modelyearid, engine, trans, mileage, month, "
            + "freq, action, item, desc, drive) VALUES "
            + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

        stmt.setInt(1, maint.id);
        stmt.setInt(2, modelYearId);
        stmt.setString(3, maint.engineCode);
        stmt.setString(4, maint.transmissionCode);
        stmt.setInt(5, maint.intervalMileage);
        stmt.setInt(6, maint.intervalMonth);
        stmt.setInt(7, maint.frequency);
        stmt.setString(8, maint.action);
        stmt.setString(9, maint.item);
        stmt.setString(10, maint.itemDescription);
        stmt.setString(11, maint.driveType);

        //System.out.println("Inserting: " + insert);
        stmt.executeUpdate();
        stmt.close();
      }

      String insertCacheTable = String.format("INSERT INTO maint_cache (modelyearid) VALUES (%d);",
            modelYearId);

      Statement stmt = conn.createStatement();
      stmt.executeUpdate(insertCacheTable);

      stmt.close();
      conn.commit();

      conn.setAutoCommit(true);
      System.out.println("Cached car " + modelYearId);
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
    }
  }
}
