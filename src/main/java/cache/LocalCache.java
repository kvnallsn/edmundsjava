package allison.kevin.cse691.car.cache;

import java.util.List;

import allison.kevin.cse691.car.models.Car;
import allison.kevin.cse691.car.models.Maintenance;

/**
 * Interface to build a cache. 
 *
 * Any class can extend this to be used as a cache
 * Example cache's included:
 * + SqliteCache: Use SQLite tables as a cache for the api
 * + NoCache: Do not use a cache
 *
 * Garage is also stored here, backed in whatever cache type is chosen
 */
public interface LocalCache {

  public List<Car> getMyGarage();

  public void addToGarage(Car car);

  public void deleteCarFromGarage(Car car);

  public Car getCarFromGarage(int id);

  public boolean checkMaintenanceCache(int modelYearId);

  public void cacheMaintenance(int modelYearId, List<Maintenance> maintList);

  public List<Maintenance> getMaintenance(int modelYearId, String[] filterCol, String[] filterArgs);

  public List<Maintenance> getMaintenance(int modelYearId);
}
