package allison.kevin.cse691.car.cache;

import java.util.ArrayList;
import java.util.List;

import allison.kevin.cse691.car.models.Car;
import allison.kevin.cse691.car.models.Maintenance;

/**
 * Represents no cache in use. 
 *
 * Returns false on any cache checks and empty lists on cache requests
 *
 * Garage is stored in memory, when this object goes out of scope, 
 * the garage is deleted
 */
public class NoCache implements LocalCache {

  private List<Car> garage;

  public NoCache() {
    garage = new ArrayList<Car>();
  }

  public List<Car> getMyGarage() {
    return garage;
  }

  public void addToGarage(Car car) {
    garage.add(car);
  }

  public void deleteCarFromGarage(Car car) {
    garage.remove(car);
  }

  public Car getCarFromGarage(int id) {
    return garage.get(id);
  }

  // No cache, always return false
  public boolean checkMaintenanceCache(int modelYearId) {
    return false;
  }

  // No cache, return empty list
  public List<Maintenance> getMaintenance(int modelYearId, String[] filterCols, String[] filterArgs) {
    return new ArrayList<Maintenance>();
  }

  // No cache, return empty list
  public List<Maintenance> getMaintenance(int modelYearId) {
    return getMaintenance(modelYearId, null, null);
  }

  public void cacheMaintenance(int modelYearId, List<Maintenance> maintList) {
    // No cache, do nothing
  }
}
