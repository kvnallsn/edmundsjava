package allison.kevin.cse691.car.models;

import java.util.List;

public class Model {
  public final String id;
  public final String name;
  public final String niceName;
  public List<Year> years;

  public Model(String id, String name, String niceName, List<Year> years) {
    this.id = id;
    this.name = name;
    this.niceName = niceName;
    this.years = years;
  }

  @Override
  public String toString() {
    return this.name;
  }
}
