package allison.kevin.cse691.car.models;

public class Engine {
  public final String id;
  public final String name;
  public final String equipmentType;
  public final String availability;
  public final float compressionRatio;
  public final int cylinder;
  public final float size;
  public final int displacement;
  public final String configuration;
  public final String fuelType;
  public final int horsepower;
  public final int torque;
  public final int totalValves;
  public final String manufacturerEngineCode;
  public final String type;
  public final String code;
  public final String compressorType;

  public Engine(String id, String name, String equipmentType, String availability, float compressionRatio,
      int cylinder, float size, int displacement, String configuration, String fuelType,
      int horsepower, int torque, int totalValves, String manufacturerEngineCode,
      String type, String code, String compressorType) {
  
    this.id = id;
    this.name = name;
    this.equipmentType = equipmentType;
    this.availability = availability;
    this.compressionRatio = compressionRatio;
    this.cylinder = cylinder;
    this.size = size;
    this.displacement = displacement;
    this.configuration = configuration;
    this.fuelType = fuelType;
    this.horsepower = horsepower;
    this.torque = torque;
    this.totalValves = totalValves;
    this.manufacturerEngineCode = manufacturerEngineCode;
    this.type = type;
    this.code = code;
    this.compressorType = compressorType;
  }
}
