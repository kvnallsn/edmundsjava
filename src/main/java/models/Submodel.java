package allison.kevin.cse691.car.models;

import java.util.List;

public class Submodel {
  public final String body;
  public final String fuel;
  public final String tuner;
  public final String modelName;
  public final String niceName;

  public Submodel(String body, String fuel, String tuner, String modelName, String niceName) {
    this.body = body;
    this.fuel = fuel;
    this.tuner = tuner;
    this.modelName = modelName;
    this.niceName = niceName;
  }

  @Override
  public String toString() {
    return this.modelName;
  }
}
