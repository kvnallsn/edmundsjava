package allison.kevin.cse691.car.models;

import allison.kevin.cse691.car.utilities.Frequency;
import allison.kevin.cse691.car.utilities.Utility;

public class Maintenance {
  public final int id;
  public final int edmundsId;
  public final String engineCode;
  public final String transmissionCode;
  public final int intervalMileage;
  public final int intervalMonth;
  public final int frequency;
  public final String action;
  public final String item;
  public final String itemDescription;
  public final float laborUnits;
  public final float partUnits;
  public final String driveType;
  public final String modelYear;
  public String linkedUuid;

  public Maintenance(int id, int edmundsId, String engineCode, String transmissionCode,
      int intervalMileage, int intervalMonth, int frequency, String action, String item, 
      String itemDescription, float laborUnits, float partUnits, String driveType,
      String modelYear, String linkedUuid) {

    this.id = id;
    this.edmundsId = edmundsId;
    this.engineCode = engineCode;
    this.transmissionCode = transmissionCode;
    this.intervalMileage = intervalMileage;
    this.intervalMonth = intervalMonth;
    this.frequency = frequency > 0 ? frequency : 0;
    this.action = action;
    this.item = item;
    this.itemDescription = itemDescription;
    this.laborUnits = laborUnits;
    this.partUnits = partUnits;
    this.driveType = driveType;
    this.modelYear = modelYear;
    this.linkedUuid = linkedUuid;
  }

  @Override
  public String toString() {
    return String.format("%s: %s", this.action, this.item);
  }

  public String interval() {
    Frequency freq = Utility.frequency(this.frequency);
    switch (freq) {
      case NONE:
        return "None";
      case BASIC:
        return "Warranty";
      case EXTENDED:
        return "Extended Warranty";
      case ONCE:
        if (this.intervalMileage != 0) {
          return String.format("Once at %,d miles", this.intervalMileage);
        } else {
          return String.format("Once at %d months", this.intervalMonth);
        }
      case FREQUENTLY:
      case EVERY:
        if (this.intervalMileage != 0) {
          return String.format("Every %,d miles", this.intervalMileage);
        } else {
          return String.format("Every %d months", this.intervalMonth);
        }
      case WARNING:
        return "On Warning (Check-Engine) Light";
      case INSPECTION1:
        return "Inspection 1";
      case INSPECTION2:
        return "Inspection 2";
      case INSPECTION3:
        return "Inspection 3";
      default:
        return "Unknown Frequency";
    }
  }
}
