package allison.kevin.cse691.car.models;

public class Car {
  public int id;
  public String uuid;
  public String make;
  public String model;
  public int year;
  public String trim;
  public int modelYearId;
  public int styleId;
  public String engine;
  public String transmission;

  // Default empty constructor for Jackson
  public Car() { }

  // Constructor for GSON
  public Car(int id, String make, String model, int year,
      String trim, int modelYearId, int styleId, String engine,
      String transmission) {

    this.id = id;
    this.make = make;
    this.model = model;
    this.year = year;
    this.trim = trim;
    this.modelYearId = modelYearId;
    this.styleId = styleId;
    this.engine = engine;
    this.transmission = transmission;
  }

  @Override
  public String toString() {
    return String.format("%d %s %s %s", year, make, model, trim);
  }

  // Getter methods
  public int getId() { return this.id; }
  public String getUuid() { return this.uuid; }
  public String getMake() { return this.make; }
  public String getModel() { return this.model; }
  public int getYear() { return this.year; }
  public String getTrim() { return this.trim; }
  public int getModelYearId() { return this.modelYearId; }
  public int getStyleId() { return this.styleId; }
  public String getEngine() { return this.engine; }
  public String getTransmission() { return this.transmission; }

  // Setter methods
  public void setId(int id) { this.id = id; }
  public void setUuid(String uuid) { this.uuid = uuid; }
  public void setMake(String make) { this.make = make; }
  public void setModel(String model) { this.model = model; }
  public void setYear(int year) { this.year = year; }
  public void setTrim(String trim) { this.trim = trim; }
  public void setModelYearId(int modelYearId) { this.modelYearId = modelYearId; }
  public void setStyleId(int styleId) { this.styleId = styleId; }
  public void setEngine(String engine) { this.engine = engine; }
  public void setTransmission(String transmission) { this.transmission = transmission; }
}

