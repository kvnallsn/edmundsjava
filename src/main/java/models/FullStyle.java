package allison.kevin.cse691.car.models;

public class FullStyle {
  public final int id;
  public final String name;
  public final Engine engine;
  public final Transmission transmission;

  public FullStyle(int id, String name, Engine engine, Transmission transmission) {
    this.id = id;
    this.name = name;
    this.engine = engine;
    this.transmission = transmission;
  }

  @Override
  public String toString() {
    return name;
  }
}

