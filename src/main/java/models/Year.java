package allison.kevin.cse691.car.models;

import java.util.ArrayList;
import java.util.List;

public class Year {
  public final int id;
  public final int year;
  public final List<Style> styles;

  public Year(int id, int year) {
    this.id = id;
    this.year  = year;
    this.styles = new ArrayList<Style>();
  }

  public Year(int id, int year, List<Style> styles) {
    this.id = id;
    this.year = year;
    this.styles = styles;
  }

  @Override
  public String toString() {
    return String.valueOf(year);
  }
}
