package allison.kevin.cse691.car.models;

public class MaintenanceNote {
  public final String note1;
  public final String note2;
  public final String style;

  public MaintenanceNote(String note1, String note2, String style) {
    this.note1 = note1;
    this.note2 = note2;
    this.style = style;
  }

  @Override
  public String toString() {
    return String.format("1) %s\n2) %s", this.note1, this.note2);
  }
}
