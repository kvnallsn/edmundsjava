package allison.kevin.cse691.car.models;

import java.util.List;

public class Make {
  public final int id;
  public final String name;
  public final String niceName;
  public final List<Model> models;

  public Make(int id, String name, String niceName, List<Model> models) {
    this.id = id;
    this.name = name;
    this.niceName = niceName;
    this.models = models;
  }

  @Override
  public String toString() {
    return this.name;
  }
}
