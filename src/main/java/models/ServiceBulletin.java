package allison.kevin.cse691.car.models;

public class ServiceBulletin {
  public final int id;
  public final String bulletinNumber;
  public final int bulletinDateMonth;
  public final int componentNumber;
  public final String componentDescription;
  public final int nhstaId;
  public final int modelYearId;
  public final String summary;

  public ServiceBulletin(int id, String bulletinNumber, int bulletinDateMonth, int componentNumber,
      String componentDescription, int nhstaId, int modelYearId, String summary) {

    this.id = id;
    this.bulletinNumber = bulletinNumber;
    this.bulletinDateMonth = bulletinDateMonth;
    this.componentNumber = componentNumber;
    this.componentDescription = componentDescription;
    this.nhstaId = nhstaId;
    this.modelYearId = modelYearId;
    this.summary = summary;
  }

  @Override
  public String toString() {
    return this.componentDescription;
  }
}
