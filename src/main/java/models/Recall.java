package allison.kevin.cse691.car.models;

public class Recall {
  public final int id;
  public final String recallNumber;
  public final String componentDescription;
  public final String manufacturerRecallNumber;
  public final String manufacturedTo;
  public final String numberOfVehiclesAffected;
  public final String influencedBy;
  public final String defectConsequence;
  public final String defectCorrectiveAction;
  public final String defectDescription;


  public Recall(int id, String recallNumber, String componentDescription,
      String manufacturerRecallNumber, String manufacturedTo, 
      String numberOfVehiclesAffected, String influencedBy, String defectConsequence,
      String defectCorrectiveAction, String defectDescription) {

    this.id = id;
    this.recallNumber = recallNumber;
    this.componentDescription = componentDescription;
    this.manufacturerRecallNumber = manufacturerRecallNumber;
    this.numberOfVehiclesAffected = numberOfVehiclesAffected;
    this.manufacturedTo = manufacturedTo;
    this.influencedBy = influencedBy;
    this.defectConsequence = defectConsequence;
    this.defectCorrectiveAction = defectCorrectiveAction;
    this.defectDescription = defectDescription;
  }

  @Override
  public String toString() {
    return this.componentDescription;
  }
}

