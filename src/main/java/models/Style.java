package allison.kevin.cse691.car.models;

public class Style {
  public final int id;
  public final String name;
  public final Submodel submodel;
  public final String trim;

  public Style(int id, String name, Submodel submodel, String trim) {
    this.id = id;
    this.name = name;
    this.submodel = submodel;
    this.trim = trim;
  }

  @Override
  public String toString() {
    return this.name;
  }
}
