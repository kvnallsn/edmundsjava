package allison.kevin.cse691.car.models;

public class Transmission {
  public final int id;
  public final String name;
  public final String equipmentType;
  public final String availability;
  public final String automaticType;
  public final String transmissionType;
  public final int numberOfSpeeds;

  public Transmission(int id, String name, String equipmentType, String availability,
      String automaticType, String transmissionType, int numberOfSpeeds) {
  
    this.id = id;
    this.name = name;
    this.equipmentType = equipmentType;
    this.availability = availability;
    this.automaticType = automaticType;
    this.transmissionType = transmissionType;
    this.numberOfSpeeds = numberOfSpeeds;
  }
}
