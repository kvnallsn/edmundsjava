package allison.kevin.cse691.car;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

import allison.kevin.cse691.car.holders.MakeHolder;
import allison.kevin.cse691.car.holders.MaintenanceHolder;

import allison.kevin.cse691.car.models.Make;
import allison.kevin.cse691.car.models.Year;
import allison.kevin.cse691.car.models.FullStyle;
import allison.kevin.cse691.car.models.Transmission;


/**
 * Edmunds API Interface, provides endpoints for different API calls
 */
public interface Edmunds {
  @GET("/api/vehicle/v2/makes?fmt=json&api_key=ebz5qcrgspwhptbmtp9bgra4")
    Call<MakeHolder> makes();

  @GET("/api/vehicle/v2/{make}?fmt=json&api_key=ebz5qcrgspwhptbmtp9bgra4")
    Call<Make> models(@Path("make") String make);

  @GET("/api/vehicle/v2/{make}/{model}/{year}?fmt=json&api_key=ebz5qcrgspwhptbmtp9bgra4")
    Call<Year> details(
        @Path("make") String make,
        @Path("model") String model,
        @Path("year") int year);

  @GET("/api/vehicle/v2/styles/{styleid}?fmt=json&api_key=ebz5qcrgspwhptbmtp9bgra4&view=full")
    Call<FullStyle> style(@Path("styleid") int id);

  @GET("/v1/api/maintenance/recallrepository/findbymodelyearid?fmt=json&api_key=ebz5qcrgspwhptbmtp9bgra4")
    Call<RecallHolder> recalls(@Query("modelyearid") String id);

  @GET("/v1/api/maintenance/servicebulletinrepository/findbymodelyearid?fmt=json&api_key=ebz5qcrgspwhptbmtp9bgra4")
    Call<ServiceBulletinHolder> bulletins(@Query("modelyearid") String id);

  @GET("/v1/api/maintenance/stylesnotes/{styleid}?fmt=json&api_key=ebz5qcrgspwhptbmtp9bgra4")
    Call<MaintenanceNotesHolder> maintenanceNotes(@Path("styleid") int id);

  @GET("/v1/api/maintenance/actionrepository/findbymodelyearid?fmt=json&api_key=ebz5qcrgspwhptbmtp9bgra4")
    Call<MaintenanceHolder> maintenance(@Query("modelyearid") int id);

  @GET("/v1/api/maintenance/action/{id}?fmt=json&api_key=ebz5qcrgspwhptbmtp9bgra4")
    Call<MaintenanceHolder> maintenanceById(@Path("id") int id);

  @GET("/v1/api/vehicle/v2/styles/{styleid}/engines?fmt=json&api_key=ebz5qcrgspwhptbmtp9bgra4")
    Call<EngineHolder> engines(@Path("styleid") int id);

  @GET("/v1/api/vehicle/v2/styles/{styleid}/transmissions?fmt=json&api_key=ebz5qcrgspwhptbmtp9bgra4")
    Call<Transmission> transmissions(@Path("styleid") int id);
}
