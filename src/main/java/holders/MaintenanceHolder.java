package allison.kevin.cse691.car.holders;

import java.util.List;

import allison.kevin.cse691.car.models.Maintenance;

public class MaintenanceHolder {
  public final List<Maintenance> actionHolder;

  public MaintenanceHolder(List<Maintenance> actionHolder) {
    this.actionHolder = actionHolder;
  }
}
