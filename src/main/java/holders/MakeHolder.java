package allison.kevin.cse691.car.holders;

import java.util.List;

import allison.kevin.cse691.car.models.Make;

public class MakeHolder {
  public List<Make> makes;

  public MakeHolder(List<Make> makes) {
    this.makes = makes;
  }
}
