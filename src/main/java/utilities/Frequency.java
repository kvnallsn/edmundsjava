package allison.kevin.cse691.car.utilities;

public enum Frequency implements EnumConverter {
  NONE(0), BASIC(1), EXTENDED(2), ONCE(3), EVERY(4), FREQUENTLY(5),
    WARNING(6), INSPECTION1(7), INSPECTION2(8), INSPECTION3(9);

  int value;

  Frequency(int value) {
    this.value = value;
  }

  public int convert() {
    return this.value;
  }
}
