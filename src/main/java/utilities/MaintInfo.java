package allison.kevin.cse691.car.utilities;

import java.util.List;
import java.util.Map;

import allison.kevin.cse691.car.models.Maintenance;

public class MaintInfo {
  public final Map<Integer, List<Maintenance>> mileage;
  public final Map<Integer, List<Maintenance>> months;

  public MaintInfo(Map<Integer, List<Maintenance>> mileage, Map<Integer, List<Maintenance>> months) {
    this.mileage = mileage;
    this.months = months;
  }
}
