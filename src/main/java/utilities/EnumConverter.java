package allison.kevin.cse691.car.utilities;

public interface EnumConverter {
  public int convert();
}

