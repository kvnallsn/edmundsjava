package allison.kevin.cse691.car.utilities;

import java.util.HashMap;
import java.util.Map;

class ReverseEnumMap<V extends Enum<V> & EnumConverter> {
  private Map<Integer, V> map = new HashMap<Integer, V>();

  public ReverseEnumMap(Class<V> valueType) {
    for (V v : valueType.getEnumConstants()) {
      map.put(v.convert(), v);
    }
  }

  public V get(int num) {
    return map.get(num);
  }
}

public class Utility {
  private static ReverseEnumMap<Frequency> reverse =
      new ReverseEnumMap<Frequency>(Frequency.class);

  public static Frequency frequency(int val) {
    return reverse.get(val);
  }
}


