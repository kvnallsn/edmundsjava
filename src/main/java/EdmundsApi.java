package allison.kevin.cse691.car;

import java.io.IOException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import allison.kevin.cse691.car.cache.LocalCache;
import allison.kevin.cse691.car.cache.NoCache;

import allison.kevin.cse691.car.holders.MakeHolder;
import allison.kevin.cse691.car.holders.MaintenanceHolder;

import allison.kevin.cse691.car.models.Make;
import allison.kevin.cse691.car.models.Model;
import allison.kevin.cse691.car.models.Year;
import allison.kevin.cse691.car.models.Style;
import allison.kevin.cse691.car.models.FullStyle;
import allison.kevin.cse691.car.models.Engine;
import allison.kevin.cse691.car.models.Transmission;
import allison.kevin.cse691.car.models.Recall;
import allison.kevin.cse691.car.models.ServiceBulletin;
import allison.kevin.cse691.car.models.MaintenanceNote;
import allison.kevin.cse691.car.models.Maintenance;

import allison.kevin.cse691.car.utilities.MaintInfo;

/**
 * Singleton class to use the Edmunds API
 */
public final class EdmundsApi {

  private static EdmundsApi myapi;

  private Edmunds edmunds;

  private EdmundsApi() {
    // Set increased timeouts because SCSI sucks
    final OkHttpClient okHttpClient = new OkHttpClient.Builder()
        .readTimeout(120, TimeUnit.SECONDS)
        .connectTimeout(120, TimeUnit.SECONDS)
        .build();


    // Create a simple REST adapter to point to API
    Retrofit retrofit = new Retrofit.Builder()
        .baseUrl(EdmundsApi.API_URL)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .build();

    // Create instance of the API interface
    edmunds = retrofit.create(Edmunds.class);
  }

  public static Edmunds getApi() {
    if (myapi == null) {
      myapi = new EdmundsApi();
    }

    return myapi.edmunds;
  }

  // Used when instantiating and instance of this class, do not remove!
  public static final String API_URL = "https://api.edmunds.com";

  // The LocalCache to use (e.g. SQLite, files, postgre...)
  // LocalCache provides a means of reducing the number of API calls
  // by storing certain information locally after the initial retrieval
  // By default, set to NoCache to not use a cache
  private static LocalCache cache = new NoCache();

  // Sets the LocalCache to be used
  public static void setCache(LocalCache myCache) {
    cache = myCache;
  }

  public static LocalCache getCache() {
    return cache;
  }

  public static List<Make> getMakes(Edmunds api) throws IOException {
    // Create a call instance for looking up API data
    Call<MakeHolder> call = api.makes();

    // Fetch and print the result of the API call
    MakeHolder makes = call.execute().body();

    return makes.makes;
  }

  public static void getMakesAsync(Edmunds api, Callback<MakeHolder> callback) {
    Call<MakeHolder> call = api.makes();
    call.enqueue(callback);
  }

  public static Model getModelInfoFromString(Edmunds api, String model) throws IOException {
    Call<Make> call = api.models("audi");
    Make make = call.execute().body();

    return getModelInfo(api, make);
  }

  public static Model getModelInfo(Edmunds api, Make make) {
    return null;
  }

  public static Year getModelYearInfo(Edmunds api, Make make, Model model, int year) throws IOException {
    Call<Year> call = api.details(make.niceName, model.niceName, year);
    Year info = call.execute().body();

    return info;
  }

  public static void getModelYearInfoAsync(Edmunds api, Make make, Model model, int year, Callback<Year> callback) {
    Call<Year> call = api.details(make.niceName, model.niceName, year);
    call.enqueue(callback);
  }

  public static FullStyle getStyleInfo(Edmunds api, int styleId) throws IOException {
    Call<FullStyle> call = api.style(styleId);
    FullStyle style = call.execute().body();

    return style;
  }

  public static void getStyleAsync(Edmunds api, int styleId, Callback<FullStyle> callback) {
    Call<FullStyle> call = api.style(styleId);
    call.enqueue(callback);
  }

  public static List<Recall> getRecalls(Edmunds api, int modelYearId) throws IOException {
    Call<RecallHolder> call = api.recalls(String.valueOf(modelYearId));
    RecallHolder holder = call.execute().body();

    return holder.recallHolder;
  }

  public static List<ServiceBulletin> getBulletins(Edmunds api, int modelYearId) throws IOException {
    Call<ServiceBulletinHolder> call = api.bulletins(String.valueOf(modelYearId));
    ServiceBulletinHolder holder = call.execute().body();

    return holder.serviceBulletinHolder;
  }

  public static List<MaintenanceNote> getMaintenanceNotes(Edmunds api, int styleId) throws IOException {
    Call<MaintenanceNotesHolder> call = api.maintenanceNotes(styleId);
    MaintenanceNotesHolder holder = call.execute().body();

    return holder.maintenanceStyleNotesHolder;
  }

  public static void getMaintenanceAsync(Edmunds api, int modelYearId, Callback<MaintenanceHolder> callback) {
    Call<MaintenanceHolder> call = api.maintenance(modelYearId);
    call.enqueue(callback);
  }

  public static MaintInfo getMaintenance(Edmunds api, int modelYearId,
      String engineCode, String transCode) throws IOException {

    boolean isCached = cache.checkMaintenanceCache(modelYearId);

    List<Maintenance> maintList;
    if (isCached) {
      maintList = cache.getMaintenance(modelYearId);
    } else {
      Call<MaintenanceHolder> call = api.maintenance(modelYearId);
      MaintenanceHolder holder = call.execute().body();

      cache.cacheMaintenance(modelYearId, holder.actionHolder);
      maintList = holder.actionHolder;
    }

    return sortMaintenance(maintList, engineCode, transCode);
  }

  public static Maintenance getMaintenanceById(Edmunds api, int id) throws IOException {
    Call<MaintenanceHolder> call = api.maintenanceById(id);

    MaintenanceHolder holder = call.execute().body();
    if (holder.actionHolder.size() == 1) {
      return holder.actionHolder.get(0);
    }

    return null;
  }

  public static MaintInfo sortMaintenance(List<Maintenance> maintList,
      String engineCode, String transCode) {

    // Put into buckets based on mileage
    Map<Integer, List<Maintenance>> buckets = new TreeMap<>();
    for (Maintenance item : maintList) {
      // Apply filters (Engine Code, Transmissions, etc)
      if (item.intervalMileage == 0) {
        continue;
      } else if (!item.engineCode.equals(engineCode)) {
        continue;
      } else if (!item.transmissionCode.equals(transCode) && !item.transmissionCode.equals("ALL")) {
        continue;
      }

      int mileage = item.intervalMileage;
      if (!buckets.containsKey(mileage)) {
        buckets.put(mileage, new ArrayList<Maintenance>());
      }

      List<Maintenance> mileageList = buckets.get(mileage);
      mileageList.add(item);
    }

    // Place remaining into month buckets
    Map<Integer, List<Maintenance>> monthBuckets = new TreeMap<>();
    for (Maintenance item : maintList) {
      if (item.intervalMonth == 0) {
        continue;
      } else if (!item.engineCode.equals(engineCode)) {
        continue;
      } else if (!item.transmissionCode.equals(transCode) && !item.transmissionCode.equals("ALL")) {
        continue;
      }

      int month = item.intervalMonth;
      if (!monthBuckets.containsKey(month)) {
        monthBuckets.put(month, new ArrayList<Maintenance>());
      }

      List<Maintenance> monthList = monthBuckets.get(month);
      monthList.add(item);
    }

    return new MaintInfo(buckets, monthBuckets);
  }

  public static Engine getEngine(Edmunds api, int styleId) throws IOException {
    Call<EngineHolder> call = api.engines(styleId);
    EngineHolder holder = call.execute().body();

    return holder.engines.get(0);
  }

  public static Transmission getTransmission(Edmunds api, int styleId) throws IOException {
    Call<Transmission> call = api.transmissions(styleId);
    Transmission trans = call.execute().body();

    return trans;
  }
}
